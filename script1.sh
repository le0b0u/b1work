#!/bin/bash
#l�o.b
#19/10/2020
#Script : affiche un r�sum� de l'OS

n=$(hostname)

echo -e "Nom de la machine : $n\n"

ip=$(ifconfig en0 | grep 'inet' | awk '{print $2}')

echo -e "IP : $ip\n"

n=$(sw_vers -productName )
v=$(sw_vers -productVersion )

echo -e "OS : $n"
echo -e "Version : $v\n"

date=$(last root)

echo -e "Date et heure d'allumage : $date"

ramtotal=$(sysctl -n hw.memsize)
ram=$(sysctl -n kern.dtrace.buffer_memory_maxsize)

echo -e "Espace Ram utilise : $ram octets"
echo -e "Espace Ram dispo : $ramtotal octets\n"
 
echo -e "Espace Disque utilise et dispo : \n"

df -h /

echo -e "\nListe des utilisateurs :\n"

dscl . list /Users | grep -v '_'

