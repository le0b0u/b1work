# Maîtrise de poste - Day 1

# I. Self-footprinting

## Host OS

```
$  hostname
MacBook-Air-de-Leo.local

$ sw_vers
ProductName:	Mac OS X
ProductVersion:	10.15.7
BuildVersion:	19H2

$ systemstats
Summary
=======
           System Version:	19H2
           Hardware Model:	MacBookAir9,1
                 Board ID:	Mac-0CFF9C7C2B63DF8D
    Installed Memory Size:	8GB (8589934592)
                Page Size:	4096
         Root Volume Type:	SSD:1,HDD:0,FUSION:0,FDE:1,RAID:0,TINYSSD:0,IN:1,EX:0,INEX:0,APFS
         Root Volume Size:	233GB (250685575168)
                 SSD Name:	APPLE SSD AP0256N
             SSD Revision:	717.120.
     Discrete GPU Present:	0
               Total Time:	293:28:48
          Dashboard Start:	2020-09-30 09:55:27
            Dashboard End:	2020-10-12 15:23:54
[...]
CPU Summary
===========
           Avg. Frequency:	1.640 GHz
           Package Power::	Pkg: 3.25W, CPU 1.64W, GPU: 0.21W
       Pkg Interrupt Rate:	Total: 2612Hz   ANS2: 47Hz   ARPT: 76Hz   IGPU: 215Hz   IOBC: 2001Hz   IPI: 1642Hz   MacBookAir9,1: 1Hz   SMC: 2Hz   TDM0: 2Hz   TMR: 557Hz   TXHC: 1Hz   XHC1: 3Hz   pci14e4,5fa0: 44Hz
          Pkg C-State Res:	Total: 152.1%   C2: 15%   C3: 0%   C6: 18%   C7: 0%   C8: 4%   C10: 115%
        Cores C-State Res:	   C6: 11%   C7: 168%
          GPU C-State Res:	   RC1: 2%   RC6: 178%
            XCPM Tier Res:	   TT2: 0%   TT3: 0%   TT5: 0%   background: 1%   idle: 91%   normal: 9%   rt_short: 0%
[...]

$ system_profiler SPMemoryDataType
Memory:

    Memory Slots:

      ECC: Disabled
      Upgradeable Memory: No

        BANK 0/ChannelA-DIMM0:

          Size: 4 GB
          Type: LPDDR4X
          Speed: 3733 MHz
          Status: OK
          Manufacturer: SK Hynix
          Part Number: H9HCNNNCRMALPR-NEE
          Serial Number: 43000000

        BANK 2/ChannelB-DIMM0:

          Size: 4 GB
          Type: LPDDR4X
          Speed: 3733 MHz
          Status: OK
          Manufacturer: SK Hynix
          Part Number: H9HCNNNCRMALPR-NEE
          Serial Number: 43000000
```

## Devices

```
$ system_profiler SPHardwareDataType
Hardware:

    Hardware Overview:

      Model Name: MacBook Air
      Model Identifier: MacBookAir9,1
      Processor Name: Quad-Core Intel Core i5
      Processor Speed: 1,1 GHz
      Number of Processors: 1
      Total Number of Cores: 4
      L2 Cache (per Core): 512 KB
      L3 Cache: 6 MB
      Hyper-Threading Technology: Enabled
      Memory: 8 GB
      Boot ROM Version: 1037.147.4.0.0 (iBridge: 17.16.16610.0.0,0)
      Serial Number (system): FVFD52G5MNHX
      Hardware UUID: 0AD52769-05EB-5518-9C01-BB76A6421E9C
      Activation Lock Status: Enabled

$ sysctl hw.logicalcpu
hw.logicalcpu: 8
```

**Intel(R) Core(TM)** -> Marque

**i5-1030NG7** -> Modèle

```
$ system_profiler SPDisplaysDataType
Graphics/Displays:

    Intel Iris Plus Graphics:

      Chipset Model: Intel Iris Plus Graphics
      Type: GPU
      Bus: Built-In
      VRAM (Dynamic, Max): 1536 MB
      Vendor: Intel
      Device ID: 0x8a51
      Revision ID: 0x0007
      Metal: Supported, feature set macOS GPUFamily2 v1
      Displays:
        Color LCD:
          Display Type: Built-In Retina LCD
          Resolution: 2560 x 1600 Retina
          Framebuffer Depth: 30-Bit Color (ARGB2101010)
          Main Display: Yes
          Mirror: Off
          Online: Yes
          Automatically Adjust Brightness: No
          Connection Type: Internal

$ system_profiler SPStorageDataType
Storage:

    Macintosh HD:

      Free: 180,07 GB (180 066 832 384 bytes)
      Capacity: 250,69 GB (250 685 575 168 bytes)
      Mount Point: /
      File System: APFS
      Writable: No
      Ignore Ownership: No
      BSD Name: disk1s1
      Volume UUID: 23C79A0D-DF71-4B38-8800-3E5F4CC050B2
      Physical Drive:
          Device Name: APPLE SSD AP0256N
          Media Name: AppleAPFSMedia
          Medium Type: SSD
          Protocol: PCI-Express
          Internal: Yes
          Partition Map Type: Unknown
          S.M.A.R.T. Status: Verified

    Macintosh HD - Data:

      Free: 180,07 GB (180 066 832 384 bytes)
      Capacity: 250,69 GB (250 685 575 168 bytes)
      Mount Point: /System/Volumes/Data
      File System: APFS
      Writable: Yes
      Ignore Ownership: No
      BSD Name: disk1s2
      Volume UUID: 56728D5D-5581-4EFC-AF7F-2CCEAF128325
      Physical Drive:
          Device Name: APPLE SSD AP0256N
          Media Name: AppleAPFSMedia
          Medium Type: SSD
          Protocol: PCI-Express
          Internal: Yes
          Partition Map Type: Unknown
          S.M.A.R.T. Status: Verified
```

## Users

```
$ dscl . list /Users                    
_amavisd
_analyticsd
_appleevents
_applepay
_appowner
_appserver
_appstore
_ard
_assetcache
_astris
_atsserver
_avbdeviced
_calendar
_captiveagent
_ces
_clamav
_cmiodalassistants
_coreaudiod
_coremediaiod
_ctkd
_cvmsroot
_cvs
_cyrus
_datadetectors
_devdocs
_devicemgr
_displaypolicyd
_distnote
_dovecot
_dovenull
_dpaudio
_driverkit
_eppc
_findmydevice
_fpsd
_ftp
_gamecontrollerd
_geod
_hidd
_iconservices
_installassistant
_installer
_jabber
_kadmin_admin
_kadmin_changepw
_krb_anonymous
_krb_changepw
_krb_kadmin
_krb_kerberos
_krb_krbtgt
_krbfast
_krbtgt
_launchservicesd
_lda
_locationd
_lp
_mailman
_mbsetupuser
_mcxalr
_mdnsresponder
_mobileasset
_mysql
_nearbyd
_netbios
_netstatistics
_networkd
_nsurlsessiond
_nsurlstoraged
_ondemand
_postfix
_postgres
_qtss
_reportmemoryexception
_sandbox
_screensaver
_scsd
_securityagent
_softwareupdate
_spotlight
_sshd
_svn
_taskgated
_teamsserver
_timed
_timezone
_tokend
_trustevaluationagent
_unknown
_update_sharing
_usbmuxd
_uucp
_warmd
_webauthserver
_windowserver
_www
_wwwproxy
_xserverdocs
daemon
leo
nobody
root

```

**Root** -> Utilisateur Full Admin

## Processus

```
$ ps aux            
USER               PID  %CPU %MEM      VSZ    RSS   TT  STAT STARTED      TIME COMMAND
_windowserver      221   4,1  1,9  7673136 158356   ??  Ss    6:23    77:41.30 /System/Library/PrivateFrameworks/SkyLight.framework/Resources/WindowServer -d
leo               6192   3,4  1,2  5160440  99444   ??  S     4:49     1:01.61 /System/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal
leo               7012   3,3  4,9 17550988 413344   ??  S     5:50     0:40.41 /Applications/Discord.app/Contents/Frameworks/Discord Helper (Renderer).app/Co
leo               4320   1,7  1,4  5508164 120200   ??  S     2:51     2:40.52 /Applications/Atom.app/Contents/MacOS/Atom
_hidd              143   1,2  0,1  4382232   9160   ??  Ss    6:23     7:00.55 /usr/libexec/hidd
leo                405   1,0  0,2  4396860  14964   ??  S     6:24     0:10.39 /System/Library/CoreServices/Siri.app/Contents/MacOS/Siri launchd
leo               4325   0,7  2,7  5521544 228480   ??  S     2:51     1:20.10 /Applications/Atom.app/Contents/Frameworks/Atom Helper.app/Contents/MacOS/Atom
root               123   0,6  0,1  4383204   8100   ??  Ss    6:23     0:23.77 /System/Library/CoreServices/launchservicesd
_coreaudiod        192   0,5  0,2  4397156  15544   ??  Ss    6:23     8:25.65 /usr/sbin/coreaudiod
leo                335   0,2  0,1  4379152   8276   ??  S     6:23     0:02.88 /System/Library/PrivateFrameworks/ViewBridge.framework/Versions/A/XPCServices/
_driverkit        6523   0,2  0,0  4801840   1700   ??  Ss    5:14     0:00.59 /System/Library/DriverExtensions/AppleUserHIDDrivers.dext/AppleUserHIDDrivers
leo               5572   0,2  2,1  9042588 179024   ??  S     3:31     5:52.27 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo                670   0,2  0,2  4902060  14016   ??  Ss    6:25     0:03.52 /System/Library/CoreServices/Siri.app/Contents/XPCServices/SiriNCService.xpc/C
root               283   0,2  0,1  4378812   6292   ??  S     6:23     0:02.52 /System/Library/PrivateFrameworks/ViewBridge.framework/Versions/A/XPCServices/
leo               7007   0,1  0,3  4588204  27912   ??  S     5:50     0:01.24 /Applications/Discord.app/Contents/Frameworks/Discord Helper.app/Contents/MacO
leo               3514   0,1  0,4  4630932  36104   ??  S     2:09     1:30.19 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo               6978   0,1  1,3  8935100 109288   ??  S     5:47     0:02.13 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo               6467   0,0  0,0  4278092    324 s000  T     5:04     0:00.00 sh -c (cd "/usr/share/man" && (echo ".ll 14.2i"; echo ".nr LL 14.2i"; /bin/cat
leo               6466   0,0  0,0  4268876    528 s000  T     5:04     0:00.01 sh -c (cd "/usr/share/man" && (echo ".ll 14.2i"; echo ".nr LL 14.2i"; /bin/cat
leo               6465   0,0  0,0  4288072   1096 s000  T     5:04     0:00.03 man thermald
leo               6444   0,0  0,0  4278684    444 s000  T     5:03     0:00.00 less
leo               6443   0,0  0,0  4279116    232 s000  T     5:03     0:00.00 sh -c (cd "/usr/share/man" && (echo ".ll 14.2i"; echo ".nr LL 14.2i"; /bin/cat
leo               6438   0,0  0,0  4279116    284 s000  T     5:03     0:00.00 sh -c (cd "/usr/share/man" && (echo ".ll 14.2i"; echo ".nr LL 14.2i"; /bin/cat
leo               6437   0,0  0,0  4268876    512 s000  T     5:03     0:00.00 sh -c (cd "/usr/share/man" && (echo ".ll 14.2i"; echo ".nr LL 14.2i"; /bin/cat
leo               6436   0,0  0,0  4289096    948 s000  T     5:03     0:00.03 man configd
leo               6403   0,0  0,0  4268444    476 s000  T     5:01     0:00.01 less
leo               6402   0,0  0,0  4280140    232 s000  T     5:01     0:00.00 sh -c (cd "/usr/share/man" && (echo ".ll 14.2i"; echo ".nr LL 14.2i"; /bin/cat
leo               6397   0,0  0,0  4278092    284 s000  T     5:01     0:00.00 sh -c (cd "/usr/share/man" && (echo ".ll 14.2i"; echo ".nr LL 14.2i"; /bin/cat
leo               6396   0,0  0,0  4268876    512 s000  T     5:01     0:00.00 sh -c (cd "/usr/share/man" && (echo ".ll 14.2i"; echo ".nr LL 14.2i"; /bin/cat
leo               6395   0,0  0,0  4297288    952 s000  T     5:01     0:00.03 man UserEventAgent
leo               6268   0,0  0,1  4900752  11044   ??  Ss    4:53     0:00.14 /System/Library/Frameworks/Quartz.framework/Versions/A/Frameworks/QuickLookUI.
leo               6267   0,0  0,4  4972276  29676   ??  Ss    4:53     0:01.07 /System/Library/Frameworks/AppKit.framework/Versions/C/XPCServices/com.apple.a
leo               6264   0,0  0,0  4273564    592 s000  T     4:52     0:00.11 less
leo               6263   0,0  0,0  4279116    236 s000  T     4:52     0:00.00 sh -c (cd "/usr/share/man" && /usr/bin/tbl '/usr/share/man/man1/top.1' | /usr/
leo               6260   0,0  0,0  4278092    296 s000  T     4:52     0:00.00 sh -c (cd "/usr/share/man" && /usr/bin/tbl '/usr/share/man/man1/top.1' | /usr/
leo               6259   0,0  0,0  4268876    516 s000  T     4:52     0:00.00 sh -c (cd "/usr/share/man" && /usr/bin/tbl '/usr/share/man/man1/top.1' | /usr/
leo               6258   0,0  0,0  4287048    948 s000  T     4:52     0:00.03 man top
leo               6194   0,0  0,0  4334616   3700 s000  S     4:49     0:00.51 -zsh
root              6193   0,0  0,0  4340748   3740 s000  Ss    4:49     0:00.03 login -pf leo
leo               6022   0,0  0,2  4400920  17520   ??  S     4:36     0:01.75 /usr/libexec/remindd
leo               5867   0,0  0,2  4607120  14172   ??  S     3:53     0:00.29 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo               5826   0,0  0,1  4875748  12152   ??  Ss    3:49     0:00.14 /System/Library/Frameworks/Metal.framework/Versions/A/XPCServices/MTLCompilerS
root              5825   0,0  0,0  4317320   2656   ??  Ss    3:49     0:00.04 /System/Library/PrivateFrameworks/AssetCacheServicesExtensions.framework/XPCSe
leo               5824   0,0  1,1  5036648  93452   ??  S     3:49     0:27.19 /System/Applications/Utilities/Activity Monitor.app/Contents/MacOS/Activity Mo
leo               5655   0,0  1,1  9203444  93960   ??  S     3:33     0:54.18 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo               5609   0,0  0,0  4334280   2524   ??  S     3:31     0:00.02 /System/Library/Frameworks/ApplicationServices.framework/Frameworks/PrintCore.
leo               5575   0,0  0,2  8906800  19724   ??  S     3:31     0:01.36 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo               5559   0,0  0,3  9192344  22968   ??  S     3:31     0:02.22 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
root              5193   0,0  0,0  4378788   4164   ??  Ss    3:14     0:00.05 /System/Library/PrivateFrameworks/SystemAdministration.framework/XPCServices/w
leo               4885   0,0  0,3  4425280  22752   ??  S     2:58     0:02.19 /usr/libexec/studentd
leo               4397   0,0  0,1  4401344  10356   ??  Ss    2:51     0:00.24 /System/Library/Frameworks/QuickLook.framework/Versions/A/XPCServices/QuickLoo
leo               4381   0,0  0,1  4874668   8632   ??  Ss    2:51     0:00.13 /System/Library/Frameworks/Metal.framework/Versions/A/XPCServices/MTLCompilerS
leo               4378   0,0  0,1  4900752  11216   ??  Ss    2:51     0:00.29 /System/Library/Frameworks/Quartz.framework/Versions/A/Frameworks/QuickLookUI.
leo               4377   0,0  0,3  4952272  22580   ??  Ss    2:51     0:02.25 /System/Library/Frameworks/AppKit.framework/Versions/C/XPCServices/com.apple.a
leo               4366   0,0  0,8  5284172  69840   ??  S     2:51     0:01.17 /Applications/Atom.app/Contents/Frameworks/Atom Helper.app/Contents/MacOS/Atom
leo               4324   0,0  0,0  4384284   3616   ??  Ss    2:51     0:00.07 /System/Library/Frameworks/VideoToolbox.framework/Versions/A/XPCServices/VTDec
leo               4323   0,0  0,0  4326364   3312   ??  S     2:51     0:00.03 /Applications/Atom.app/Contents/Frameworks/Electron Framework.framework/Resour
leo               4321   0,0  0,3  4688652  29216   ??  S     2:51     0:11.18 /Applications/Atom.app/Contents/Frameworks/Atom Helper.app/Contents/MacOS/Atom
root              4319   0,0  0,1  4351028   8300   ??  Ss    2:50     0:00.65 /System/Library/CoreServices/backupd.bundle/Contents/Resources/backupd
leo               3965   0,0  0,0  4289632   1240   ??  S     2:42     0:00.03 /usr/bin/ssh-agent -l
leo               3803   0,0  0,1  4381016   5144   ??  Ss    2:22     0:00.30 /System/Library/PrivateFrameworks/Categories.framework/Versions/A/XPCServices/
leo               3802   0,0  0,1  4437360  11152   ??  Ss    2:22     0:00.26 /Applications/Utilities/Adobe Sync/CoreSync/Core Sync.app/Contents/PlugIns/ACC
leo               3801   0,0  0,2  4406012  13180   ??  Ss    2:22     0:00.25 /Applications/OneDrive.app/Contents/PlugIns/FinderSync.appex/Contents/MacOS/Fi
leo               3800   0,0  0,1  4850092   8632   ??  Ss    2:22     0:00.09 /System/Library/Frameworks/Metal.framework/Versions/A/XPCServices/MTLCompilerS
leo               3797   0,0  0,1  4900752  11192   ??  Ss    2:22     0:00.32 /System/Library/Frameworks/Quartz.framework/Versions/A/Frameworks/QuickLookUI.
leo               3796   0,0  0,3  4985752  27908   ??  Ss    2:22     0:03.68 /System/Library/Frameworks/AppKit.framework/Versions/C/XPCServices/com.apple.a
leo               3792   0,0  0,1  4850016   8388   ??  Ss    2:22     0:00.05 /System/Library/Frameworks/Metal.framework/Versions/A/XPCServices/MTLCompilerS
leo               3791   0,0  0,1  4859140   8512   ??  Ss    2:22     0:00.05 /System/Library/Frameworks/Metal.framework/Versions/A/XPCServices/MTLCompilerS
_applepay         3789   0,0  0,0  4379800   3620   ??  Ss    2:22     0:00.12 /usr/libexec/seld
leo               3788   0,0  0,1  4382216   6396   ??  S     2:22     0:00.13 /System/Library/PrivateFrameworks/ClassKit.framework/Versions/A/progressd
leo               3784   0,0  0,2  4391008  13692   ??  S     2:22     0:00.96 /System/Library/PrivateFrameworks/PassKitCore.framework/passd
root              3782   0,0  0,0  4352252   3900   ??  Ss    2:22     0:00.45 /System/Library/PrivateFrameworks/AppStoreDaemon.framework/Support/appstored
leo               3778   0,0  0,1  4379980  10696   ??  S     2:22     0:00.16 /System/Library/PrivateFrameworks/CloudPhotoServices.framework/Versions/A/XPCS
leo               3775   0,0  0,1  4396200  11796   ??  S     2:22     0:00.23 /System/Library/PrivateFrameworks/CommerceKit.framework/Versions/A/Resources/s
leo               3762   0,0  0,0  4334172   2720   ??  S     2:21     0:00.02 /System/Library/Frameworks/CoreServices.framework/Frameworks/Metadata.framewor
leo               3759   0,0  0,1  4848900   8588   ??  Ss    2:21     0:00.10 /System/Library/Frameworks/Metal.framework/Versions/A/XPCServices/MTLCompilerS
leo               3568   0,0  0,0  4832784   1536   ??  Ss    2:12     0:00.07 /System/Library/Frameworks/Metal.framework/Versions/A/XPCServices/MTLCompilerS
leo               3527   0,0  2,0  8930796 164116   ??  S     2:09     0:31.52 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo               3518   0,0  0,1  4389844   6508   ??  Ss    2:09     0:00.12 /System/Library/Frameworks/VideoToolbox.framework/Versions/A/XPCServices/VTDec
leo               3517   0,0  0,0  4342464   2604   ??  Ss    2:09     0:00.02 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo               3513   0,0  2,0  5141628 163944   ??  S     2:09    11:18.60 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo               3504   0,0  0,0  4325576   2788   ??  S     2:09     0:00.02 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo               3502   0,0  2,8  5329660 231024   ??  S     2:09    17:46.53 /Applications/Google Chrome.app/Contents/MacOS/Google Chrome
_gamecontrollerd  3470   0,0  0,1  4350664   4372   ??  Ss    2:08     0:01.06 /usr/libexec/gamecontrollerd
root              3387   0,0  0,1  4380952   4360   ??  Ss    1:55     0:00.07 /System/Library/Frameworks/SystemExtensions.framework/Versions/A/Helpers/sysex
leo               3384   0,0  0,1  4908652  10756   ??  S     1:55     0:00.13 /System/Library/PrivateFrameworks/UniversalAccess.framework/Versions/A/Resourc
leo               3350   0,0  0,1  4378272   8160   ??  S     1:53     0:00.08 /System/Library/CoreServices/rcd.app/Contents/MacOS/rcd
_netbios          3303   0,0  0,0  4352380   3512   ??  SNs   1:53     0:00.31 /usr/sbin/netbiosd
root              3290   0,0  0,0  4378368   2792   ??  Ss    1:52     0:00.99 /usr/sbin/ocspd
leo               3284   0,0  0,0  4379224   2676   ??  Ss    1:52     0:00.08 /System/Library/SystemConfiguration/EAPOLController.bundle/Contents/Resources/
root              3190   0,0  0,0  4315416   2092   ??  Ss   12:39     0:00.04 /usr/libexec/applessdstatistics
root              3077   0,0  0,0  4301256    328   ??  SNs  12:15     0:00.02 /usr/libexec/periodic-wrapper weekly
leo               3076   0,0  0,2  4394540  18592   ??  S    12:14     0:04.15 /System/Library/PrivateFrameworks/PhotoAnalysis.framework/Versions/A/Support/p
leo               3002   0,0  0,1  4385468   6836   ??  S    12:14     0:00.16 /System/Library/PrivateFrameworks/AMPLibrary.framework/Versions/A/Support/AMPA
leo               3001   0,0  0,1  4379056   6152   ??  S    12:14     0:00.14 /System/Library/PrivateFrameworks/BookKit.framework/Versions/A/XPCServices/com
leo               3000   0,0  0,1  4380176   5800   ??  S    12:14     0:00.20 /System/Library/PrivateFrameworks/PodcastServices.framework/XPCServices/Podcas
leo               2999   0,0  0,1  4405856   6888   ??  Ss   12:14     0:00.14 /System/Library/PrivateFrameworks/AssistantServices.framework/Versions/A/XPCSe
root              2666   0,0  0,0  4378484   2088   ??  Ss    4:28     0:00.09 /usr/libexec/microstackshot
root              2537   0,0  0,0  4297160    328   ??  SNs   3:42     0:00.02 /usr/libexec/periodic-wrapper daily
root              2536   0,0  0,0  4379916   2784   ??  Ss    3:42     0:00.14 /System/Library/PrivateFrameworks/Noticeboard.framework/Versions/A/Resources/n
leo               2535   0,0  0,2  4399680  12996   ??  S     3:42     0:00.85 /System/Library/PrivateFrameworks/Noticeboard.framework/Versions/A/Resources/n
leo               2534   0,0  0,1  4379924   6804   ??  Ss    3:42     0:00.16 /System/Library/PrivateFrameworks/AssistantServices.framework/Versions/A/XPCSe
leo               2527   0,0  0,1  4383580   4708   ??  Ss    3:42     0:00.55 /System/Library/PrivateFrameworks/Categories.framework/Versions/A/XPCServices/
leo               2495   0,0  0,1  4391988   6228   ??  Ss    3:41     0:00.81 /System/Library/PrivateFrameworks/MediaConversionService.framework/Versions/A/
leo               2398   0,0  0,1  4380676   4968   ??  S     1:41     0:00.87 /System/Library/Frameworks/CoreServices.framework/Frameworks/Metadata.framewor
_spotlight        2397   0,0  0,1  4386380   6144   ??  S     1:41     0:06.28 /System/Library/Frameworks/CoreServices.framework/Frameworks/Metadata.framewor
leo               2379   0,0  0,1  4380500   4832   ??  S     1:40     0:00.29 /usr/libexec/silhouette
root              2376   0,0  0,0  4380080   3480   ??  Ss    1:39     0:00.09 /usr/bin/sysdiagnose
leo               2347   0,0  0,1  4384200   8840   ??  S     1:38     0:02.29 cloudphotod
leo               2337   0,0  0,0  4380384   3804   ??  S     1:38     0:00.23 /System/Library/CoreServices/ScopedBookmarkAgent
leo               2333   0,0  0,2  4438208  18928   ??  S     1:38     0:22.70 /System/Library/PrivateFrameworks/PhotoLibraryServices.framework/Versions/A/Su
root              2309   0,0  0,0  4299364   2588   ??  Ss    1:06     0:00.04 /System/Library/CoreServices/ReportCrash daemon
leo               2173   0,0  0,1  4333708   7660   ??  S    12:13     0:00.04 /System/Library/CoreServices/EscrowSecurityAlert.app/Contents/MacOS/EscrowSecu
_fpsd             2171   0,0  0,0  4353360   2400   ??  Ss   12:12     0:00.54 /System/Library/PrivateFrameworks/CoreLSKD.framework/Versions/A/lskdd
root              2115   0,0  0,0  4384856   3452   ??  Ss   11:49     0:00.20 /usr/libexec/dprivacyd
leo               1792   0,0  0,0  4369904   3608   ??  Ss    9:54     0:00.06 /System/Library/PrivateFrameworks/FMClient.framework/Versions/A/XPCServices/FM
leo               1783   0,0  0,0  4325444   1540   ??  S     9:54     0:00.17 /System/Library/PrivateFrameworks/DeviceCheckInternal.framework/devicecheckd
leo               1666   0,0  0,2  4407968  13284   ??  S     9:14     0:01.09 /System/Library/CoreServices/PowerChime.app/Contents/MacOS/PowerChime
leo               1573   0,0  0,0  4351972   2580   ??  S     9:02     0:00.07 /usr/libexec/USBAgent
leo               1572   0,0  0,2  4912764  12996   ??  S     9:02     0:00.39 /System/Library/Frameworks/LocalAuthentication.framework/Support/coreautha.bun
root              1488   0,0  0,0  4381396   3460   ??  S     8:17     0:00.07 /usr/libexec/secinitd
root              1487   0,0  0,1  4393460  10224   ??  S     8:17     0:03.95 /System/Library/PrivateFrameworks/GeoServices.framework/Versions/A/XPCServices
leo               1485   0,0  0,2  4926396  14916   ??  S     8:15     0:10.27 /System/Library/CoreServices/LocationMenu.app/Contents/MacOS/LocationMenu
leo               1378   0,0  0,1  4379768   5540   ??  S     7:33     0:00.79 /System/Library/PrivateFrameworks/UsageTracking.framework/Versions/A/UsageTrac
leo               1273   0,0  0,0  4334052   1388   ??  S     6:59     0:00.01 /System/Library/PrivateFrameworks/ToneLibrary.framework/Versions/A/XPCServices
root              1250   0,0  0,0  4380456   4076   ??  Ss    6:58     0:00.30 /System/Library/PrivateFrameworks/PerformanceAnalysis.framework/Versions/A/XPC
leo               1248   0,0  0,0  4334092   1764   ??  S     6:58     0:00.03 /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks
leo               1247   0,0  0,1  4383708  12072   ??  S     6:58     0:02.50 /System/Library/PrivateFrameworks/WeatherFoundation.framework/Versions/A/XPCSe
leo               1089   0,0  0,1  4918652  10888   ??  S     6:51     0:00.72 /System/Library/CoreServices/OSDUIHelper.app/Contents/MacOS/OSDUIHelper
_assetcache       1079   0,0  0,1  4382580   5048   ??  Ss    6:49     0:00.64 /System/Library/PrivateFrameworks/AssetCacheServices.framework/Versions/A/XPCS
leo               1078   0,0  0,1  4383480   7848   ??  S     6:49     0:00.40 /usr/libexec/keyboardservicesd
leo               1077   0,0  0,8  4694640  63480   ??  S     6:49     0:08.81 /System/Library/Services/AppleSpell.service/Contents/MacOS/AppleSpell
leo               1040   0,0  0,1  4385108   5728   ??  S     6:38     0:00.40 /usr/libexec/transparencyd
leo               1039   0,0  0,1  4378916   8296   ??  S     6:38     0:00.20 /System/Library/PrivateFrameworks/IMDPersistence.framework/IMAutomaticHistoryD
root              1021   0,0  0,0  4350676   2692   ??  Ss    6:29     0:00.04 /usr/libexec/tzd
root              1000   0,0  0,1  4356008   5528   ??  Ss    6:28     0:00.81 /usr/libexec/mobileactivationd
leo                994   0,0  0,4  4397424  30816   ??  S     6:28     0:13.89 /System/Library/PrivateFrameworks/AssistantServices.framework/Versions/A/Suppo
leo                993   0,0  0,3  4473660  21252   ??  Ss    6:28     0:06.95 /System/Library/PrivateFrameworks/AssistantServices.framework/Versions/A/XPCSe
leo                992   0,0  0,2  4428220  15924   ??  S     6:28     0:07.17 /System/Library/PrivateFrameworks/AppStoreDaemon.framework/Support/appstoreage
leo                990   0,0  0,1  4656288   5348   ??  S     6:28     0:00.52 /System/Library/Frameworks/CoreSpotlight.framework/CoreSpotlightService
leo                989   0,0  0,1  4391268   8340   ??  S     6:28     0:00.92 /usr/libexec/adprivacyd
leo                981   0,0  0,1  4438384  11284   ??  Ss    6:28     0:00.48 /Applications/Utilities/Adobe Sync/CoreSync/Core Sync.app/Contents/PlugIns/ACC
leo                977   0,0  0,5  4999996  38940   ??  S     6:28     1:57.50 /Applications/Utilities/Adobe Sync/CoreSync/Core Sync.app/Contents/MacOS/Core
leo                951   0,0  0,0  4334016   2560   ??  Ss    6:27     0:00.02 /System/Library/Frameworks/AudioToolbox.framework/XPCServices/com.apple.audio.
_windowserver      915   0,0  0,1  4883716  10600   ??  Ss    6:27     0:00.29 /System/Library/Frameworks/Metal.framework/Versions/A/XPCServices/MTLCompilerS
root               896   0,0  0,1  4380608   5420   ??  S     6:27     0:00.24 /Library/Application Support/Adobe/Adobe Desktop Common/ElevationManager/Adobe
root               887   0,0  0,1  4379668   5792   ??  Ss    6:27     0:00.23 /Library/PrivilegedHelperTools/com.adobe.acc.installer.v2
leo                885   0,0  0,4  4669156  34636   ??  S     6:26     0:52.99 /Applications/Utilities/Adobe Creative Cloud Experience/CCXProcess/CCXProcess.
leo                876   0,0  0,1  4379328   5356   ??  S     6:26     0:33.89 /Library/Application Support/Adobe/Adobe Desktop Common/ADS/Adobe Desktop Serv
leo                874   0,0  0,4  4701744  31820   ??  S     6:26     0:13.60 /Library/Application Support/Adobe/Creative Cloud Libraries/CCLibrary.app/Cont
leo                872   0,0  0,2  4421888  19804   ??  S     6:26     0:12.23 /Applications/Utilities/Adobe Creative Cloud/ACC/Creative Cloud Helper.app/Con
leo                859   0,0  0,7  5765852  60644   ??  S     6:26     1:27.22 /Library/Application Support/Adobe/Adobe Desktop Common/ADS/Adobe Desktop Serv
leo                856   0,0  0,1  4415844  11616   ??  S     6:26     0:36.98 /Library/Application Support/Adobe/Adobe Desktop Common/IPCBox/AdobeIPCBroker.
root               839   0,0  0,0  4315088   1004   ??  Ss    6:26     0:00.01 /System/Library/Frameworks/Security.framework/authtrampoline
leo                672   0,0  0,0  4315584   2560   ??  Ss    6:25     0:00.02 /System/Library/Frameworks/AudioToolbox.framework/XPCServices/com.apple.audio.
leo                671   0,0  0,3  4946224  28440   ??  S     6:25     0:01.71 /System/Library/Frameworks/ApplicationServices.framework/Frameworks/SpeechSynt
root               666   0,0  0,0  4352108   1848   ??  Ss    6:25     0:00.11 /System/Library/CoreServices/iconservicesagent runAsRoot
root               640   0,0  0,0  4349892   3348   ??  Ss    6:24     0:00.04 /System/Library/PrivateFrameworks/PackageKit.framework/Resources/system_instal
leo                639   0,0  0,1  4353820   6384   ??  S     6:24     0:00.16 /System/Library/PrivateFrameworks/CommerceKit.framework/Versions/A/Resources/s
root               638   0,0  0,0  4349892   3380   ??  Ss    6:24     0:00.04 /System/Library/PrivateFrameworks/PackageKit.framework/Resources/installd
leo                637   0,0  0,1  4385928   8584   ??  S     6:24     0:02.45 /System/Library/PrivateFrameworks/FileProvider.framework/Support/fileproviderd
leo                635   0,0  0,1  4352380   6512   ??  S     6:24     0:00.21 /usr/libexec/replayd
root               632   0,0  0,0  4350420   2312   ??  Ss    6:24     0:00.10 /System/Library/PrivateFrameworks/CacheDelete.framework/deleted_helper
leo                627   0,0  0,1  4455228   6288   ??  S     6:24     0:00.16 /System/Library/PrivateFrameworks/CoreSuggestions.framework/Versions/A/Support
leo                625   0,0  0,1  4379368   7356   ??  S     6:24     0:00.40 /System/Library/PrivateFrameworks/CoreRecents.framework/Versions/A/Support/rec
leo                624   0,0  0,1  4417312   4972   ??  S     6:24     0:08.70 /System/Library/CoreServices/ReportCrash agent
leo                597   0,0  0,0  4378628   2652   ??  S     6:24     0:00.05 /System/Library/Frameworks/ColorSync.framework/Support/colorsync.useragent
_atsserver         595   0,0  0,0  4381312   2652   ??  Ss    6:24     0:00.40 /System/Library/Frameworks/ApplicationServices.framework/Frameworks/ATS.framew
root               588   0,0  0,1  4382076   5004   ??  Ss    6:24     0:20.57 /System/Library/PrivateFrameworks/StorageKit.framework/Resources/storagekitd
_spotlight         585   0,0  0,1  4382548   7376   ??  S     6:24     0:01.36 /usr/libexec/trustd --agent
leo                579   0,0  0,1  4378868   4212   ??  S     6:24     0:00.12 /System/Library/PrivateFrameworks/CommunicationsFilter.framework/CMFSyncAgent
leo                575   0,0  0,0  4333672   2236   ??  S     6:24     0:00.03 /usr/libexec/spindump_agent
leo                574   0,0  0,5  4515552  40004   ??  S     6:24     0:12.50 /Library/Apple/System/Library/CoreServices/SafariSupport.bundle/Contents/MacOS
leo                573   0,0  0,1  4379608   8000   ??  S     6:24     0:00.35 /usr/libexec/siriknowledged
leo                570   0,0  0,2  4386756  13632   ??  S     6:24     0:02.17 /System/Library/PrivateFrameworks/CallHistory.framework/Support/CallHistorySyn
root               566   0,0  0,1  4382084   4740   ??  Ss    6:24     0:00.68 /System/Library/PrivateFrameworks/AuthKit.framework/Versions/A/Support/akd
leo                562   0,0  0,0  4352096   1888   ??  S     6:24     0:00.05 /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks
_locationd         561   0,0  0,0  4351608   1044   ??  S     6:24     0:00.19 /usr/sbin/distnoted agent
_fpsd              547   0,0  0,0  4379020   2644   ??  Ss    6:24     0:00.19 /System/Library/PrivateFrameworks/CoreFP.framework/Versions/A/fpsd
leo                544   0,0  0,1  4395348  11060   ??  S     6:24     0:00.68 /usr/libexec/avconferenced
root               543   0,0  0,0  4350004   1844   ??  Ss    6:24     0:00.04 /System/Library/CoreServices/CrashReporterSupportHelper server-init
_applepay          542   0,0  0,1  4381100   4204   ??  Ss    6:24     0:00.91 /usr/libexec/nfcd
root               541   0,0  0,0  4380636   3260   ??  Ss    6:24     0:00.17 /System/Library/PrivateFrameworks/CoreAccessories.framework/Support/accessoryd
leo                538   0,0  0,1  4351652   6280   ??  S     6:24     0:00.42 /usr/libexec/findmydevice-user-agent
leo                535   0,0  0,0  4350196   3764   ??  S     6:24     0:00.06 /System/Library/PrivateFrameworks/CoreFollowUp.framework/Versions/A/Support/fo
root               530   0,0  0,1  4403944   8128   ??  Ss    6:24     0:00.78 /usr/sbin/spindump
root               527   0,0  0,0  4350860   3532   ??  Ss    6:24     0:00.12 /System/Library/CoreServices/Software Update.app/Contents/Resources/suhelperd
_softwareupdate    526   0,0  0,2  4479028  12692   ??  Ss    6:24     0:41.09 /System/Library/CoreServices/Software Update.app/Contents/Resources/softwareup
leo                525   0,0  0,1  4404932  11312   ??  S     6:24     0:00.44 /System/Library/PrivateFrameworks/SoftwareUpdate.framework/Resources/SoftwareU
leo                522   0,0  0,1  4486152  11680   ??  S     6:24     0:02.28 /System/Library/PrivateFrameworks/AMPLibrary.framework/Versions/A/Support/AMPL
leo                519   0,0  0,1  4382152   5960   ??  S     6:24     0:19.16 /System/Library/PrivateFrameworks/CacheDelete.framework/deleted
root               517   0,0  0,2  4386460  14428   ??  Ss    6:24     0:00.88 /usr/libexec/rtcreportingd
root               516   0,0  0,0  4353068   3108   ??  Ss    6:24     0:01.84 /System/Library/PrivateFrameworks/CoreSymbolication.framework/coresymbolicatio
root               514   0,0  0,0  4350848   3752   ??  Ss    6:24     0:00.36 /System/Library/Frameworks/AudioToolbox.framework/XPCServices/CAReportingServi
_datadetectors     513   0,0  0,0  4351440   1744   ??  Ss    6:24     0:00.09 /usr/libexec/DataDetectorsSourceAccess
leo                512   0,0  0,1  4379944   7336   ??  Ss    6:24     0:00.17 /System/Library/PrivateFrameworks/AssistantServices.framework/Versions/A/XPCSe
leo                511   0,0  0,1  4380652   5104   ??  S     6:24     0:00.20 /System/Library/Frameworks/AudioToolbox.framework/AudioComponentRegistrar
_nsurlstoraged     506   0,0  0,0  4351544   2520   ??  Ss    6:24     0:00.13 /usr/libexec/nsurlstoraged --privileged
leo                494   0,0  0,0  4351100   1940   ??  S     6:24     0:00.02 /usr/libexec/loginitemregisterd
root               488   0,0  0,0  4380388   2576   ??  Ss    6:24     0:00.08 /System/Library/PrivateFrameworks/AssetCacheServicesExtensions.framework/XPCSe
_assetcache        487   0,0  0,1  4382196   4816   ??  Ss    6:24     0:00.18 /usr/libexec/AssetCache/AssetCache
leo                486   0,0  0,1  4384724   5564   ??  S     6:24     0:00.75 /System/Library/PrivateFrameworks/AssetCacheServices.framework/Versions/A/XPCS
leo                482   0,0  0,1  4383276   4348   ??  S     6:24     0:00.13 /System/Library/CoreServices/Menu Extras/SafeEjectGPUExtra.menu/Contents/XPCSe
leo                481   0,0  0,0  4830696   2276   ??  S     6:24     0:00.01 SafeEjectGPUAgent
root               478   0,0  0,1  4378752   5236   ??  Ss    6:24     0:01.53 /System/Library/PrivateFrameworks/AmbientDisplay.framework/Versions/A/XPCServi
leo                475   0,0  0,1  4379488   7176   ??  Ss    6:24     0:00.27 /System/Library/PrivateFrameworks/IMFoundation.framework/XPCServices/IMRemoteU
leo                473   0,0  0,1  4380780   7544   ??  Ss    6:24     0:00.83 /System/Library/PrivateFrameworks/IMFoundation.framework/XPCServices/IMRemoteU
leo                472   0,0  0,1  4379488   8060   ??  Ss    6:24     0:00.31 /System/Library/PrivateFrameworks/IMFoundation.framework/XPCServices/IMRemoteU
leo                470   0,0  0,6  4556644  48060   ??  S     6:24     0:29.32 /System/Library/PrivateFrameworks/CoreSuggestions.framework/Versions/A/Support
_captiveagent      469   0,0  0,0  4352684   2612   ??  Ss    6:24     0:00.15 /usr/libexec/captiveagent
leo                464   0,0  0,1  4388100   9808   ??  S     6:24     0:00.51 /System/Library/PrivateFrameworks/FamilyCircle.framework/Versions/A/Resources/
leo                463   0,0  0,2  4845124  14892   ??  S     6:24     0:12.42 /System/Library/Frameworks/CoreServices.framework/Frameworks/Metadata.framewor
root               462   0,0  0,1  4396528  10752   ??  Ss    6:24     0:01.33 /usr/libexec/findmydeviced
leo                460   0,0  0,1  4384084   7592   ??  Ss    6:24     0:00.61 /System/Library/PrivateFrameworks/CloudDocsDaemon.framework/XPCServices/Contai
leo                459   0,0  0,0  4858820   3932   ??  S     6:24     0:00.15 /usr/libexec/SidecarRelay
leo                458   0,0  0,1  4349988   4324   ??  S     6:24     0:00.22 /System/Library/PrivateFrameworks/CoreCDP.framework/Versions/A/Resources/cdpd
_fpsd              457   0,0  0,1  4354160   4840   ??  Ss    6:24     0:00.93 /System/Library/PrivateFrameworks/CoreADI.framework/adid
leo                454   0,0  0,1  4902724   9628   ??  S     6:24     0:00.32 /System/Library/PrivateFrameworks/AppSSO.framework/Support/AppSSOAgent.app/Con
leo                450   0,0  0,1  4386912  11596   ??  S     6:24     0:01.75 /usr/libexec/fmfd
leo                449   0,0  0,0  4349992   2480   ??  S     6:24     0:00.33 /System/Library/PrivateFrameworks/MediaRemote.framework/Support/mediaremoteage
leo                448   0,0  0,1  4397040  10640   ??  S     6:24     0:00.41 /System/Library/CoreServices/CoreLocationAgent.app/Contents/MacOS/CoreLocation
root               447   0,0  0,1  4366988   5160   ??  Ss    6:24     0:22.46 /System/Library/PrivateFrameworks/XprotectFramework.framework/Versions/A/XPCSe
leo                441   0,0  0,2  4391652  20636   ??  Ss    6:24     0:10.61 /System/Library/PrivateFrameworks/CalendarNotification.framework/Versions/A/XP
leo                440   0,0  0,1  4386384  11084   ??  S     6:24     0:01.95 /System/Library/PrivateFrameworks/CallHistory.framework/Support/CallHistoryPlu
leo                438   0,0  0,0  4379092   3112   ??  Ss    6:24     0:00.05 /System/Library/PrivateFrameworks/Categories.framework/Versions/A/XPCServices/
root               437   0,0  0,1  4380964   5076   ??  Ss    6:24     0:01.02 /usr/libexec/wifivelocityd
leo                436   0,0  0,0  4378248   3592   ??  S     6:24     0:00.05 /usr/libexec/WiFiVelocityAgent
leo                435   0,0  0,2  4410888  13344   ??  Ss    6:24     0:01.23 /Applications/OneDrive.app/Contents/PlugIns/FinderSync.appex/Contents/MacOS/Fi
leo                434   0,0  0,1  4391636   9724   ??  S     6:24     0:11.81 /System/Library/PrivateFrameworks/GeoServices.framework/Versions/A/XPCServices
leo                433   0,0  0,1  4390544  11700   ??  S     6:24     0:01.69 /System/Library/PrivateFrameworks/CommerceKit.framework/Versions/A/Resources/c
leo                430   0,0  0,0  4379140   3044   ??  Ss    6:24     0:00.11 /System/Library/PrivateFrameworks/Categories.framework/Versions/A/XPCServices/
leo                429   0,0  0,0  4379044   3984   ??  S     6:24     0:00.25 /System/Library/CoreServices/pbs
leo                428   0,0  0,4  4904540  30144   ??  Ss    6:24     0:00.84 /System/Library/CoreServices/Dock.app/Contents/XPCServices/com.apple.dock.extr
leo                427   0,0  0,0  4351648   2784   ??  S     6:24     0:00.06 /System/Library/Frameworks/CryptoTokenKit.framework/ctkahp.bundle/Contents/Mac
leo                426   0,0  0,4  5035704  33192   ??  S     6:24     0:39.05 /Applications/OneDrive.app/Contents/MacOS/OneDrive
leo                425   0,0  0,1  4381044   5772   ??  S     6:24     0:08.10 /usr/libexec/swcd
leo                424   0,0  0,1  4897964   9028   ??  S     6:24     0:00.48 /System/Library/Frameworks/QuickLookThumbnailing.framework/Support/com.apple.q
leo                423   0,0  0,1  4352204   7232   ??  S     6:24     0:00.21 /System/Library/PrivateFrameworks/CommerceKit.framework/Versions/A/Resources/s
root               422   0,0  0,0  4351612   2508   ??  Ss    6:24     0:00.06 /System/Library/Frameworks/CryptoTokenKit.framework/ctkahp.bundle/Contents/Mac
leo                420   0,0  0,1  4381248   9056   ??  S     6:24     0:02.51 /System/Library/PrivateFrameworks/AppleMediaServices.framework/Resources/amsac
leo                418   0,0  0,1  4926888  11980   ??  S     6:24     0:01.73 /System/Library/CoreServices/TextInputMenuAgent.app/Contents/MacOS/TextInputMe
leo                417   0,0  0,1  4379480   6008   ??  S     6:24     0:01.02 /System/Library/CoreServices/diagnostics_agent
leo                414   0,0  0,1  4380252   9568   ??  S     6:24     0:18.54 /System/Library/CoreServices/cloudpaird
leo                412   0,0  0,1  4900980   9492   ??  S     6:24     0:00.25 /System/Library/CoreServices/AirPlayUIAgent.app/Contents/MacOS/AirPlayUIAgent
leo                411   0,0  0,1  4382396  10088   ??  S     6:24     0:14.74 /Library/Application Support/Adobe/AdobeGCClient/AGMService -mode=logon
leo                409   0,0  0,1  4380552   7332   ??  S     6:24     0:01.89 /System/Library/PrivateFrameworks/AskPermission.framework/Versions/A/Resources
leo                408   0,0  0,4  4419256  34656   ??  S     6:24     0:38.32 /System/Library/PrivateFrameworks/CalendarAgent.framework/Executables/Calendar
leo                406   0,0  0,1  4379456   4852   ??  S     6:24     0:00.25 /System/Library/Image Capture/Support/icdd
leo                404   0,0  0,1  4382020   6656   ??  S     6:24     0:00.33 /System/Library/Frameworks/InputMethodKit.framework/Resources/imklaunchagent
leo                401   0,0  0,1  4378080   4484   ??  S     6:24     0:00.17 /System/Library/CoreServices/SocialPushAgent.app/Contents/MacOS/SocialPushAgen
leo                397   0,0  0,1  4383212  11748   ??  S     6:24     0:01.29 /System/Library/PrivateFrameworks/CoreSpeech.framework/corespeechd
leo                395   0,0  0,1  4386960   9172   ??  S     6:24     0:26.15 /System/Library/PrivateFrameworks/UserActivity.framework/Agents/useractivityd
leo                394   0,0  0,2  4392332  13304   ??  S     6:24     0:02.15 /System/Library/PrivateFrameworks/CoreParsec.framework/parsecd
leo                393   0,0  0,5  4412268  38068   ??  S     6:24     1:10.49 /usr/libexec/sharingd
leo                387   0,0  0,3  4423208  22736   ??  S     6:24     0:05.85 /System/Library/PrivateFrameworks/IMDPersistence.framework/XPCServices/IMDPers
leo                385   0,0  0,0  4386040   4168   ??  S     6:24     0:00.17 /usr/libexec/neagent
leo                384   0,0  0,1  4379068  11448   ??  S     6:24     0:30.22 /System/Library/Frameworks/AddressBook.framework/Executables/ContactsAccountsS
leo                382   0,0  0,1  4393548   8784   ??  S     6:24     0:02.15 /System/Library/PrivateFrameworks/AuthKit.framework/Versions/A/Support/akd
leo                381   0,0  0,2  4383004  13032   ??  S     6:23     0:00.85 /System/Library/PrivateFrameworks/IMCore.framework/imagent.app/Contents/MacOS/
leo                379   0,0  0,2  4391976  19096   ??  S     6:23     0:03.32 /System/Library/PrivateFrameworks/TelephonyUtilities.framework/callservicesd
_ctkd              377   0,0  0,0  4343916   1920   ??  Ss    6:23     0:00.03 /System/Library/Frameworks/CryptoTokenKit.framework/ctkd -s
root               376   0,0  0,1  4379292   4264   ??  Ss    6:23     0:00.51 /usr/sbin/WirelessRadioManagerd
leo                375   0,0  0,1  4380572   4228   ??  S     6:23     0:00.10 /System/Library/Frameworks/CryptoTokenKit.framework/ctkd -tw
leo                374   0,0  0,3  4411708  23884   ??  S     6:23     0:10.80 /System/Library/PrivateFrameworks/AssistantServices.framework/Versions/A/Suppo
leo                372   0,0  0,2  4398820  12588   ??  S     6:23     0:09.71 /System/Library/CoreServices/WiFiAgent.app/Contents/MacOS/WiFiAgent
leo                371   0,0  0,2  4382996  15156   ??  S     6:23     0:01.31 /System/Library/PrivateFrameworks/HomeKitDaemon.framework/Support/homed
leo                370   0,0  0,1  4383764   8424   ??  S     6:23     0:02.49 /System/Library/CoreServices/mapspushd
leo                369   0,0  0,1  4385236   8828   ??  S     6:23     0:00.49 /System/Library/Frameworks/Security.framework/Versions/A/XPCServices/TrustedPe
leo                368   0,0  0,1  4385040  11252   ??  S     6:23     0:03.04 /System/Library/PrivateFrameworks/CoreDuetContext.framework/Resources/ContextS
leo                367   0,0  0,2  4422920  14032   ??  S     6:23     0:00.60 /System/Library/CoreServices/CoreServicesUIAgent.app/Contents/MacOS/CoreServic
root               366   0,0  0,1  4356688   4572   ??  Ss    6:23     0:00.29 /usr/sbin/filecoordinationd
leo                363   0,0  0,2  4389228  18700   ??  S     6:23     0:04.81 /System/Library/PrivateFrameworks/ExchangeSync.framework/Versions/Current/exch
leo                362   0,0  0,0  4325896   1724   ??  S     6:23     0:00.01 /System/Library/CoreServices/APFSUserAgent
leo                361   0,0  0,1  4391648  12192   ??  S     6:23     0:04.65 /System/Library/Frameworks/CoreTelephony.framework/Support/CommCenter -L
leo                360   0,0  0,1  4384408   4404   ??  Ss    6:23     0:00.05 /System/Library/PrivateFrameworks/Categories.framework/Versions/A/XPCServices/
leo                359   0,0  0,2  4392952  14908   ??  S     6:23     0:13.53 /System/Library/PrivateFrameworks/ScreenTimeCore.framework/Versions/A/ScreenTi
leo                358   0,0  0,4  4962248  32536   ??  S     6:23     0:05.71 /System/Library/CoreServices/NotificationCenter.app/Contents/MacOS/Notificatio
leo                357   0,0  0,1  4381212   6576   ??  S     6:23     0:12.52 /System/Library/CoreServices/lockoutagent
leo                356   0,0  0,2  4399392  16092   ??  S     6:23     0:05.50 /System/Library/PrivateFrameworks/IDS.framework/identityservicesd.app/Contents
leo                352   0,0  0,2  4470368  17440   ??  S     6:23     0:03.05 /System/Library/CoreServices/iconservicesagent
leo                350   0,0  0,1  4381192   5736   ??  S     6:23     0:00.11 /System/Library/PrivateFrameworks/AMPDevices.framework/Versions/A/Support/AMPD
leo                348   0,0  0,1  4380388   4304   ??  S     6:23     0:00.17 /usr/libexec/networkserviceproxy
leo                346   0,0  0,1  4388604   9412   ??  S     6:23     0:01.07 /usr/sbin/usernoted
leo                344   0,0  0,3  4403904  25680   ??  S     6:23     1:56.96 /usr/libexec/routined LAUNCHED_BY_LAUNCHD
leo                342   0,0  0,1  4381092  11456   ??  S     6:23     0:02.98 /usr/libexec/rapportd
leo                341   0,0  0,1  4451520   4812   ??  S     6:23     0:00.93 /System/Library/PrivateFrameworks/ContextKit.framework/Versions/A/XPCServices/
leo                339   0,0  0,1  4382880   9644   ??  S     6:23     0:15.55 /System/Library/Frameworks/ApplicationServices.framework/Frameworks/ATS.framew
leo                338   0,0  0,0  4379204   3464   ??  Ss    6:23     0:00.42 /System/Library/PrivateFrameworks/Categories.framework/Versions/A/XPCServices/
leo                337   0,0  0,5  5001200  43476   ??  S     6:23     0:09.29 /System/Library/CoreServices/Spotlight.app/Contents/MacOS/Spotlight
leo                336   0,0  0,2  4387156  13444   ??  S     6:23     0:02.20 /System/Library/PrivateFrameworks/CloudDocsDaemon.framework/Versions/A/Support
leo                333   0,0  0,1  4384632   8764   ??  S     6:23     0:00.96 /usr/libexec/dmd
leo                332   0,0  0,0  4350812   3236   ??  S     6:23     0:00.16 /System/Library/Frameworks/Security.framework/Versions/A/Resources/CloudKeycha
leo                331   0,0  0,7  4580388  58000   ??  S     6:23     1:06.90 /System/Library/Frameworks/Accounts.framework/Versions/A/Support/accountsd
leo                330   0,0  0,1  4387140   7460   ??  S     6:23     0:03.48 /usr/libexec/nsurlstoraged
leo                329   0,0  0,1  4381952   8080   ??  S     6:23     0:05.73 /usr/libexec/secinitd
leo                328   0,0  0,1  4385620  10388   ??  S     6:23     0:06.39 /usr/libexec/pkd
leo                327   0,0  0,1  4900752  11248   ??  Ss    6:23     0:00.47 /System/Library/Frameworks/Quartz.framework/Versions/A/Frameworks/QuickLookUI.
root               326   0,0  0,1  4390624   4408   ??  Ss    6:23     0:00.70 /usr/sbin/systemsoundserverd
leo                325   0,0  0,1  4351456   4464   ??  S     6:23     0:00.90 /usr/libexec/pboard
leo                322   0,0  0,7  5081840  59456   ??  S     6:23     0:41.34 /System/Library/CoreServices/Finder.app/Contents/MacOS/Finder
leo                321   0,0  0,5  4956576  39556   ??  S     6:23     0:40.99 /System/Library/CoreServices/SystemUIServer.app/Contents/MacOS/SystemUIServer
leo                320   0,0  1,1  5068280  90040   ??  S     6:23     0:26.39 /System/Library/CoreServices/Dock.app/Contents/MacOS/Dock
leo                319   0,0  0,1  4404372   9608   ??  S     6:23     0:00.42 /System/Library/CoreServices/talagent
leo                318   0,0  0,1  4397064  11712   ??  S     6:23     0:12.44 /usr/libexec/nsurlsessiond
leo                317   0,0  0,1  4387792   9288   ??  S     6:23     0:03.22 /System/Library/PrivateFrameworks/TCC.framework/Resources/tccd
leo                316   0,0  0,1  4380956   6640   ??  S     6:23     0:00.70 /System/Library/CoreServices/sharedfilelistd
leo                315   0,0  0,1  4380960   4884   ??  S     6:23     0:00.12 /System/Library/CoreServices/backgroundtaskmanagementagent
leo                313   0,0  0,3  4409760  23168   ??  S     6:23     0:22.61 /System/Library/PrivateFrameworks/CloudKitDaemon.framework/Support/cloudd
leo                311   0,0  0,2  4395308  20360   ??  S     6:23     0:47.61 /usr/libexec/trustd --agent
leo                310   0,0  0,1  4389248  12480   ??  S     6:23     0:17.68 /usr/libexec/secd
leo                309   0,0  0,2  4389316  12692   ??  S     6:23     0:02.68 /usr/libexec/lsd
leo                307   0,0  0,2  4396288  14660   ??  S     6:23     0:08.41 /usr/libexec/knowledge-agent
leo                306   0,0  0,1  4351608   4560   ??  S     6:23     0:08.82 /usr/sbin/distnoted agent
leo                305   0,0  0,1  4389784   9604   ??  S     6:23     0:00.93 /System/Library/PrivateFrameworks/CloudServices.framework/Helpers/com.apple.sb
leo                303   0,0  0,2  4380928  17340   ??  S     6:23     0:28.92 /usr/libexec/UserEventAgent (Aqua)
root               302   0,0  0,0  4378596   3104   ??  Ss    6:23     0:00.18 /usr/libexec/biokitaggdd
root               301   0,0  0,1  4379552   7264   ??  Ss    6:23     0:00.84 /usr/libexec/biometrickitd --launchd
root               300   0,0  0,0  4335644   1340   ??  Ss    6:23     0:00.19 /usr/libexec/securityd_service
leo                299   0,0  0,0  4350712   3520   ??  S     6:23     0:10.29 /usr/sbin/cfprefsd agent
leo                298   0,0  0,1  4378432   4976   ??  S     6:23     0:00.19 /System/Library/Frameworks/LocalAuthentication.framework/Support/coreauthd
root               297   0,0  0,1  4379560   6676   ??  Ss    6:23     0:00.29 /System/Library/Frameworks/LocalAuthentication.framework/Support/coreauthd
root               294   0,0  0,0  4318028   1352   ??  Ss    6:23     0:00.07 /System/Library/Frameworks/GSS.framework/Helpers/GSSCred
root               290   0,0  0,0  4350532    920   ??  Ss    6:23     0:00.02 /System/Library/Frameworks/CoreMediaIO.framework/Versions/A/XPCServices/com.ap
root               289   0,0  0,0  4350500   2136   ??  Ss    6:23     0:00.04 /System/Library/PrivateFrameworks/AccountPolicy.framework/XPCServices/com.appl
root               287   0,0  0,0  4381076   3500   ??  S     6:23     0:00.42 /usr/sbin/systemstats --logger-helper /private/var/db/systemstats
root               285   0,0  0,0  4351608   1088   ??  S     6:23     0:00.19 /usr/sbin/distnoted agent
root               281   0,0  0,1  4351196   5836   ??  Ss    6:23     1:26.29 /usr/libexec/sysmond
root               280   0,0  0,0  4323560   3072   ??  Ss    6:23     0:00.02 /usr/libexec/bootinstalld
root               279   0,0  0,1  4379184   5076   ??  Ss    6:23     0:00.67 /System/Library/CoreServices/SubmitDiagInfo server-init
_softwareupdate    278   0,0  0,0  4350584   2060   ??  Ss    6:23     0:00.16 /System/Library/PrivateFrameworks/BridgeOSSoftwareUpdate.framework/Support/bos
_appleevents       276   0,0  0,1  4381312   4968   ??  Ss    6:23     0:00.42 /System/Library/CoreServices/appleeventsd --server
_cmiodalassistants   275   0,0  0,1  4395372   5204   ??  Ss    6:23     0:00.33 /System/Library/Frameworks/CoreMediaIO.framework/Resources/VDC.plugin/Contents
root               274   0,0  0,0  4308976   2368   ??  Ss    6:23     0:00.28 /usr/libexec/thermald
root               273   0,0  0,0  4379548   3448   ??  Ss    6:23     0:00.61 /usr/libexec/usbd
root               272   0,0  0,0  4341792   2236   ??  Ss    6:23     0:00.23 /usr/libexec/apfsd
root               271   0,0  0,0  4333612   2484   ??  Ss    6:23     0:00.03 /System/Library/CryptoTokenKit/com.apple.ifdreader.slotd/Contents/MacOS/com.ap
root               270   0,0  0,0  4307992   1012   ??  Ss    6:23     0:00.01 /usr/libexec/multiversed
root               269   0,0  0,0  4327168   1268   ??  Ss    6:23     0:00.02 /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/CVMServer
root               268   0,0  0,0  4351188   1368   ??  Ss    6:23     0:00.03 /usr/libexec/colorsyncd
root               267   0,0  0,0  4350028   2544   ??  Ss    6:23     0:00.05 /usr/libexec/colorsync.displayservices
root               266   0,0  0,0  4381396   3524   ??  Ss    6:23     0:00.08 /usr/libexec/secinitd
root               264   0,0  1,2  5297736 101280   ??  Ss    6:23     1:19.70 /System/Library/Frameworks/CoreServices.framework/Frameworks/Metadata.framewor
root               263   0,0  0,0  4353748   2880   ??  Ss    6:23     0:00.31 /System/Library/Frameworks/Security.framework/Versions/A/XPCServices/com.apple
root               258   0,0  0,1  4384912   8352   ??  Ss    6:23     0:03.32 /usr/libexec/runningboardd
_driverkit         257   0,0  0,0  4799288    692   ??  Ss    6:23     0:00.01 /System/Library/DriverExtensions/AppleUserHIDDrivers.dext/AppleUserHIDDrivers
_driverkit         255   0,0  0,0  4805412   1120   ??  Ss    6:23     0:00.20 /System/Library/DriverExtensions/AppleUserHIDDrivers.dext/AppleUserHIDDrivers
_driverkit         251   0,0  0,0  4805412   1080   ??  Ss    6:23     0:00.14 /System/Library/DriverExtensions/AppleUserHIDDrivers.dext/AppleUserHIDDrivers
_driverkit         250   0,0  0,0  4799268    708   ??  Ss    6:23     0:00.03 /System/Library/DriverExtensions/AppleUserHIDDrivers.dext/AppleUserHIDDrivers
root               248   0,0  0,0  4379676   3208   ??  Ss    6:23     0:00.09 /usr/libexec/smd
_coreaudiod        245   0,0  0,0  4305084   2556   ??  Ss    6:23     0:00.01 /System/Library/Frameworks/AudioToolbox.framework/XPCServices/com.apple.audio.
root               244   0,0  0,0  4344096   2516   ??  Ss    6:23     0:00.05 /System/Library/Frameworks/AudioToolbox.framework/AudioComponentRegistrar -dae
root               243   0,0  0,0  4353720   4172   ??  Ss    6:23     0:00.54 /usr/libexec/diskmanagementd
root               241   0,0  0,1  4378684   9272   ??  Ss    6:23     0:00.58 /System/Library/CoreServices/backupd.bundle/Contents/Resources/backupd-helper
root               234   0,0  0,1  4385912   9628   ??  Ss    6:23     0:03.68 /usr/libexec/lsd runAsRoot
root               233   0,0  0,2  4392860  15220   ??  Ss    6:23     1:18.65 /usr/libexec/airportd
root               232   0,0  0,1  4384068   6788   ??  Ss    6:23     0:00.29 /usr/libexec/searchpartyd
_locationd         231   0,0  0,0  4382580   3660   ??  S     6:23     0:00.27 /usr/libexec/trustd --agent
_locationd         230   0,0  0,0  4334040    700   ??  S     6:23     0:00.01 /usr/sbin/cfprefsd agent
_locationd         229   0,0  0,0  4381472   3524   ??  S     6:23     0:00.11 /usr/libexec/secinitd
_nsurlsessiond     228   0,0  0,1  4387976   9044   ??  Ss    6:23     0:04.53 /usr/libexec/nsurlsessiond --privileged
_locationd         227   0,0  0,1  4388372   8092   ??  S     6:23     0:04.62 /System/Library/PrivateFrameworks/GeoServices.framework/Versions/A/XPCServices
root               225   0,0  0,0  4379708   2792   ??  Ss    6:23     0:00.14 /System/Library/PrivateFrameworks/WirelessDiagnostics.framework/Support/awdd
root               222   0,0  0,0  4379140   2500   ??  Ss    6:23     0:02.68 /usr/sbin/mDNSResponderHelper
_networkd          216   0,0  0,1  4393320  10936   ??  Ss    6:23     0:17.59 /usr/libexec/symptomsd
_mdnsresponder     209   0,0  0,1  4382552   5600   ??  Ss    6:23     0:06.73 /usr/sbin/mDNSResponder
_coreaudiod        205   0,0  0,0  4378772   2720   ??  Ss    6:23     0:00.07 /System/Library/Frameworks/CoreAudio.framework/Versions/A/XPCServices/com.appl
root               204   0,0  0,2  4386552  19112   ??  Ss    6:23     0:09.87 /usr/libexec/mobileassetd
root               191   0,0  0,1  4388448  10068   ??  Ss    6:23     0:22.71 /usr/libexec/trustd
root               186   0,0  0,1  4380252   4364   ??  Ss    6:23     0:00.56 /usr/libexec/nehelper
root               181   0,0  0,1  4386448  10760   ??  Ss    6:23     0:17.91 /System/Library/PrivateFrameworks/CoreDuetContext.framework/Resources/contexts
_analyticsd        179   0,0  0,1  4386292  10640   ??  Ss    6:23     0:08.97 /System/Library/PrivateFrameworks/CoreAnalytics.framework/Support/analyticsd
leo                159   0,0  0,4  4453324  30940   ??  Ss    6:23     0:05.61 /System/Library/CoreServices/loginwindow.app/Contents/MacOS/loginwindow consol
root               156   0,0  0,1  4382676   5312   ??  Ss    6:23     0:01.79 /System/Library/Frameworks/Security.framework/Versions/A/XPCServices/authd.xpc
root               154   0,0  0,1  4388828  10812   ??  Ss    6:23     0:06.42 /System/Library/PrivateFrameworks/TCC.framework/Resources/tccd system
root               153   0,0  0,0  4324916    376   ??  Ss    6:23     0:00.04 aslmanager
root               152   0,0  0,0  4350520   3728   ??  Ss    6:23     0:10.47 /usr/sbin/cfprefsd daemon
root               151   0,0  0,1  4404492   4496   ??  Ss    6:23     0:01.78 /System/Library/CoreServices/coreservicesd
root               150   0,0  0,2  4418572  19184   ??  Ss    6:23     1:28.01 /usr/libexec/syspolicyd
root               149   0,0  0,1  4405404   7220   ??  Ss    6:23     0:03.76 /usr/libexec/amfid
_distnote          148   0,0  0,0  4351608   1916   ??  Ss    6:23     0:01.35 /usr/sbin/distnoted daemon
root               147   0,0  0,0  4350648   1676   ??  Ss    6:23     0:14.72 /usr/sbin/notifyd
root               146   0,0  0,1  4382276   6604   ??  Ss    6:23     0:11.49 /usr/libexec/AirPlayXPCHelper
root               145   0,0  0,1  4385264   7508   ??  Ss    6:23     0:12.89 /usr/libexec/corebrightnessd --launchd
root               144   0,0  0,2  4545792  19856   ??  Ss    6:23     0:04.80 /usr/libexec/sandboxd
root               142   0,0  0,1  4382952   8884   ??  Ss    6:23     1:47.96 /usr/sbin/bluetoothd
root               140   0,0  0,0  4325584    960   ??  Ss    6:23     0:00.01 /usr/sbin/KernelEventAgent
root               139   0,0  0,0  4381064   3132   ??  Ss    6:23     0:00.32 /System/Library/PrivateFrameworks/GenerationalStorage.framework/Versions/A/Sup
root               138   0,0  0,0  4350008   3224   ??  Ss    6:23     0:00.07 /System/Library/CoreServices/logind
root               136   0,0  0,2  4386380  19620   ??  Ss    6:23     0:30.05 /usr/libexec/PerfPowerServices
root               134   0,0  0,1  4384928   9024   ??  Ss    6:23     0:20.10 /usr/libexec/dasd
_displaypolicyd    133   0,0  0,0  4353316   3872   ??  Ss    6:23     0:00.36 /usr/libexec/displaypolicyd -k 1
root               132   0,0  0,0  4317776   1528   ??  Ss    6:23     0:00.01 autofsd
_locationd         129   0,0  0,2  4397120  12720   ??  Ss    6:23     0:21.31 /usr/libexec/locationd
root               127   0,0  0,0  4325312   1004   ??  Ss    6:23     0:00.01 auditd -l
root               126   0,0  0,1  4385584   7072   ??  Ss    6:23     0:13.94 /usr/sbin/securityd -i
_usbmuxd           125   0,0  0,0  4378904   2700   ??  Ss    6:23     0:00.21 /System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/Resources/
_timed             124   0,0  0,0  4378556   4096   ??  Ss    6:23     0:01.34 /usr/libexec/timed
root               122   0,0  0,1  4396392  10952   ??  Ss    6:23     0:04.57 /System/Library/PrivateFrameworks/ApplePushService.framework/apsd
root               121   0,0  0,1  4406548  12192   ??  Ss    6:23     0:25.50 /usr/libexec/opendirectoryd
root               117   0,0  0,2  4392756  18536   ??  Ss    6:23     0:15.89 /usr/libexec/coreduetd
root               114   0,0  0,1  4378064   4720   ??  Ss    6:23     0:04.01 /usr/libexec/diskarbitrationd
_iconservices      113   0,0  0,0  4351996   2824   ??  Ss    6:23     0:00.19 /System/Library/CoreServices/iconservicesd
root               112   0,0  0,5  4514264  43216   ??  Ss    6:23     1:32.67 /System/Library/Frameworks/CoreServices.framework/Frameworks/Metadata.framewor
root               108   0,0  0,0  4350580   3288   ??  Ss    6:23     0:03.10 /usr/libexec/watchdogd
root               105   0,0  0,0  4350376   2276   ??  Ss    6:23     0:00.25 /usr/libexec/keybagd -t 15
root               104   0,0  0,3  4455832  26536   ??  Ss    6:23     1:02.45 /usr/libexec/logd
root               100   0,0  0,1  4380120   7100   ??  Ss    6:23     0:24.87 /System/Library/CoreServices/powerd.bundle/powerd
root                99   0,0  0,0  4350960   1084   ??  Ss    6:23     0:00.04 endpointsecurityd
root                98   0,0  0,1  4386540   7944   ??  Ss    6:23     0:11.91 /usr/libexec/configd
root                97   0,0  0,2  4390080  14116   ??  Ss    6:23     0:56.56 /usr/sbin/systemstats --daemon
root                94   0,0  0,2  4388132  13868   ??  Ss    6:23     0:02.38 /System/Library/PrivateFrameworks/MediaRemote.framework/Support/mediaremoted
root                93   0,0  0,0  4428312   4192   ??  Ss    6:23     1:03.28 /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/FSEven
root                92   0,0  0,1  4891508  12384   ??  Ss    6:23     0:11.67 /usr/libexec/kextd
root                91   0,0  0,0  4309204   1988   ??  Ss    6:23     0:01.93 /System/Library/PrivateFrameworks/Uninstall.framework/Resources/uninstalld
root                88   0,0  0,1  4381380  11144   ??  Ss    6:23     0:15.51 /usr/libexec/UserEventAgent (System)
root                87   0,0  0,0  4351120   1080   ??  Ss    6:23     0:05.30 /usr/sbin/syslogd
root              7028   0,0  0,0  4268800    984 s000  R+    5:52     0:00.01 ps aux
root                 1   0,0  0,2  4357524  14504   ??  Ss    6:23     1:33.29 /sbin/launchd
leo               7027   0,0  0,1  4383908   8772   ??  Ss    5:51     0:00.11 /System/Library/PrivateFrameworks/CloudDocs.framework/PlugIns/com.apple.CloudD
leo               7014   0,0  0,2  4555020  17508   ??  S     5:50     0:00.11 /Applications/Discord.app/Contents/Frameworks/Discord Helper.app/Contents/MacO
leo               7011   0,0  0,0  4316324   3688   ??  S     5:50     0:00.03 /Applications/Discord.app/Contents/Frameworks/Electron Framework.framework/Res
leo               7006   0,0  0,2  4391000  14504   ??  Ss    5:50     0:00.57 /System/Library/Frameworks/VideoToolbox.framework/Versions/A/XPCServices/VTDec
leo               7005   0,0  1,2  4750340  98460   ??  S     5:50     0:13.30 /Applications/Discord.app/Contents/Frameworks/Discord Helper (GPU).app/Content
leo               7004   0,0  1,1  5640344  93564   ??  S     5:50     0:08.90 /Applications/Discord.app/Contents/MacOS/Discord
leo               7002   0,0  0,2  4359576  17064   ??  S     5:50     0:00.36 /System/Library/Frameworks/CoreServices.framework/Frameworks/Metadata.framewor
leo               6982   0,0  0,4  8860016  30852   ??  S     5:48     0:00.12 /Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.fr
leo               6948   0,0  0,0  4315632   2156   ??  S     5:46     0:00.02 /System/Library/Frameworks/MediaAccessibility.framework/Versions/A/XPCServices
leo               6832   0,0  0,0  4278684    892 s000  T     5:34     0:00.01 more /etc/services
_driverkit        6528   0,0  0,0  4799268   1512   ??  Ss    5:14     0:00.01 /System/Library/DriverExtensions/AppleUserHIDDrivers.dext/AppleUserHIDDrivers
_driverkit        6527   0,0  0,0  4802340   1744   ??  Ss    5:14     0:00.07 /System/Library/DriverExtensions/AppleUserHIDDrivers.dext/AppleUserHIDDrivers
_driverkit        6524   0,0  0,0  4801316   1740   ??  Ss    5:14     0:33.49 /System/Library/DriverExtensions/AppleUserHIDDrivers.dext/AppleUserHIDDrivers
_driverkit        6522   0,0  0,0  4801316   1608   ??  Ss    5:14     0:00.02 /System/Library/DriverExtensions/AppleUserHIDDrivers.dext/AppleUserHIDDrivers
leo               6473   0,0  0,0  4277660    388 s000  T     5:04     0:00.00 less
leo               6472   0,0  0,0  4280140    248 s000  T     5:04     0:00.00 sh -c (cd "/usr/share/man" && (echo ".ll 14.2i"; echo ".nr LL 14.2i"; /bin/cat

```

### launchd
* Démon de gestion des services d'initialisation et de système d'exploitation créé par Apple Inc

### syslogd

* Protocole définissant un service de journaux d'événements d'un système informatique.

### UserEventAgent

* Démon qui charge les plug-ins fournis par le système pour gérer les événements système de haut niveau qui ne peuvent pas être surveillés directement par launchd (8).

### configd

* Démon responsable de nombreux aspects de configuration du système local. configd conserve les données reflétant l'état actuel du système, fournit des notifications aux applications lorsque ces données changent et héberge un certain nombre d'agents de configuration sous la forme de paquets chargeables.

### thermald

* Outil mis au point par Intel pour mesurer et contrôler la température des processeurs intel récents et leur éviter de surchauffer.

## Network

```
$ system_profiler SPNetworkDataType 
Network:

    USB 10/100/1000 LAN:

      Type: Ethernet
      Hardware: Ethernet
      BSD Device Name: en5
      IPv4:
          Configuration Method: DHCP
      IPv6:
          Configuration Method: Automatic
      Proxies:
          Exceptions List: *.local, 169.254/16
          FTP Passive Mode: Yes
      Service Order: 0

    Wi-Fi:

      Type: AirPort
      Hardware: AirPort
      BSD Device Name: en0
      IPv4 Addresses: 192.168.0.11
      IPv4:
          AdditionalRoutes:
              DestinationAddress: 192.168.0.11
              SubnetMask: 255.255.255.255
              DestinationAddress: 169.254.0.0
              SubnetMask: 255.255.0.0
          Addresses: 192.168.0.11
          ARPResolvedHardwareAddress: 00:10:18:de:ad:05
          ARPResolvedIPAddress: 192.168.0.1
          Configuration Method: DHCP
          ConfirmedInterfaceName: en0
          Interface Name: en0
          Network Signature: IPv4.Router=192.168.0.1;IPv4.RouterHardwareAddress=00:10:18:de:ad:05
          Router: 192.168.0.1
          Subnet Masks: 255.255.255.0
      IPv6:
          Configuration Method: Automatic
      DNS:
          Server Addresses: 89.2.0.1, 89.2.0.2
      DHCP Server Responses:
          Domain Name Servers: 89.2.0.1,89.2.0.2
          Lease Duration (seconds): 0
          DHCP Message Type: 0x05
          Routers: 192.168.0.1
          Server Identifier: 192.168.0.1
          Subnet Mask: 255.255.255.0
      Ethernet:
          MAC Address: 14:7d:da:45:b9:a9
          Media Options: 
          Media Subtype: Auto Select
      Proxies:
          Exceptions List: *.local, 169.254/16
          FTP Passive Mode: Yes
      Service Order: 1

    Bluetooth PAN:

      Type: Ethernet
      Hardware: Ethernet
      BSD Device Name: en4
      IPv4:
          Configuration Method: DHCP
      IPv6:
          Configuration Method: Automatic
      Proxies:
          Exceptions List: *.local, 169.254/16
          FTP Passive Mode: Yes
      Service Order: 2

    Thunderbolt Bridge:

      Type: Ethernet
      Hardware: Ethernet
      BSD Device Name: bridge0
      IPv4:
          Configuration Method: DHCP
      IPv6:
          Configuration Method: Automatic
      Proxies:
          Exceptions List: *.local, 169.254/16
          FTP Passive Mode: Yes
      Service Order: 3
```

**Carte USB** -> Permet de brancher un outil composé d'un port USB sur la machine.

**Carte Wifi** -> Permet la transmission de données numériques sans fil.

**Carte Bluetooth** -> Permet de connecter deux appareils sur une courte distance.

**Carte Thunderbolt Bridge** -> Permet le transfert de données, la sortie vidéo et l'alimentation de la machine.

```
$ more /etc/services
[...]
tcpmux            1/udp     # TCP Port Service Multiplexer
tcpmux            1/tcp     # TCP Port Service Multiplexer
#                          Mark Lottor <MKL@nisc.sri.com>
[...]
compressnet       2/udp     # Management Utility
compressnet       2/tcp     # Management Utility
compressnet       3/udp     # Compression Process
compressnet       3/tcp     # Compression Process
[...]
#                 4/tcp    Unassigned
#                 4/udp    Unassigned
rje               5/udp     # Remote Job Entry
rje               5/tcp     # Remote Job Entry
[...]
#                 6/tcp    Unassigned
#                 6/udp    Unassigned
echo              7/udp     # Echo
echo              7/tcp     # Echo
[...]
#                 8/tcp    Unassigned
#                 8/udp    Unassigned
```

### tcpmux

* Ce protocole permet d'accéder à un autre protocole internet en utilisant son nom au lieu de son numéro de port assigné

### compressnet

* Protocole utilisé pour compresser les connexions TCP pour WANS.

### rje

* Procédure d'envoi de demandes de tâches de traitement de données non interactives aux ordinateurs centraux à partir de postes de travail distants

### echo

*  Teste et mesure les temps d' aller-retour dans les réseaux IP.

# II. Scripting

# III. Gestion de softs

Les gestionnaires de paquets sont des outils automatisant le processus d'installation, désinstallation, mise à jour de logiciels installés sur un système informatique.

C'est beaucoup plus rapide et sécurisé que le téléchargement en direct sur internet.

```
$ brew list
docbook			liblqr			p11-kit
docbook-xsl		libomp			pcre
freetype		libpng			pixman
gdbm			libssh2			pkg-config
gettext			libtasn1		protobuf
ghostscript		libtiff			python@3.8
glib			libtool			qemu
gmp			libunistring		qt
gnu-getopt		libusb			readline
gnutls			little-cms2		shared-mime-info
go			memcached		sqlite
icu4c			mysql			unbound
ilmbase			ncurses			vde
imagemagick		nettle			webp
jpeg			node			x265
jq			oniguruma		xmlto
libde265		openexr			xz
libevent		openjpeg		yarn
libffi			openssl
libheif			openssl@1.1

$ brew info wget
wget: stable 1.20.3 (bottled), HEAD
Internet file retriever
https://www.gnu.org/software/wget/
Not installed
From: https://github.com/Homebrew/homebrew-core/blob/HEAD/Formula/wget.rb
License: GPL-3.0-or-later
==> Dependencies
Build: pkg-config ✘
Required: libidn2 ✘, openssl@1.1 ✔
==> Options
--HEAD
	Install HEAD version
==> Analytics
install: 93,106 (30 days), 287,630 (90 days), 1,284,189 (365 days)
install-on-request: 91,918 (30 days), 283,895 (90 days), 1,240,223 (365 days)
build-error: 0 (30 days)

```

# IV. Machine virtuelle

## Configuration

```

$ ifup enp0s8

$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:49:fe:06 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 85929sec preferred_lft 85929sec
    inet6 fe80::50dd:4bf2:24fc:6c7f/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:8c:e5:a4 brd ff:ff:ff:ff:ff:ff
    inet 192.168.120.52/24 brd 192.168.120.255 scope global noprefixroute dynamic enp0s8
       valid_lft 162sec preferred_lft 162sec
    inet6 fe80::513d:6118:c990:b353/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

$ ssh root@192.168.120.52
The authenticity of host '192.168.120.52 (192.168.120.52)' can't be established.
ECDSA key fingerprint is SHA256:1LHgB095tjO70YVF+vuJ8sSpE0Rt4goCnvAUyhiA+NY.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '192.168.120.52' (ECDSA) to the list of known hosts.
root@192.168.120.52's password: 
Last login: Fri Nov 13 18:12:55 2020
[root@localhost ~]# 

```